public class SimpleWar{
	public static void main(String[] args){
		int score1 = 0;
		int score2 = 0;
		
		Deck gameDeck = new Deck();
		gameDeck.shuffle();
		
		int rounds = 1;
		
		while(gameDeck.length() > 0){
			//draws card from shuffled deck
			Card player1 = gameDeck.drawTopCard();
			Card player2 = gameDeck.drawTopCard();
			
			//current round
			System.out.print("\u001B[33m" + "\n" + "Round " + rounds + "\u001B[0m" + "\n");
		
			//score before
			System.out.print("\n" + "Scores Before: " + "\n"
				+ "Player 1: " + score1 + "\n"
				+ "Player 2: " + score2 + "\n");
			
			//card + score
			System.out.print("\n" + "Cards: " + "\n"
				+ "Player 1: " + player1 + " (" + player1.calculateScore() + ") " 
				+ "\n" + " vs " + "\n" 
				+ "Player 2: " + player2 + " (" + player2.calculateScore() + ") " + "\n");
			
			//who wins
			if(player1.calculateScore() >= player2.calculateScore()){
				System.out.print("\n" + "Player 1 wins!" + " (" + player1.calculateScore() + " vs " + player2.calculateScore() + ") " + "\r\n");
				score1++;
			}
			else{
				System.out.print("\n" + "Player 2 wins!" + " (" + player1.calculateScore() + " vs " + player2.calculateScore() + ") " + "\r\n");
				score2++;
			}
			
			//score before
			System.out.print("\r\n" + "Scores After: " + "\n"
				+ "Player 1: " + score1 + "\n" 
				+ "Player 2: " + score2 + "\n");
			
			rounds++;
		}
		
		//calculates winner or tie
		if(score1 == score2)
			System.out.println("\u001B[32m" + "\n" + "It's a tie!" + "\u001B[0m");
		else if(score1 >= score2)
			System.out.println("\u001B[32m" + "\n" + "Congrats Player 1! You have won!" + "\u001B[0m");
		else
			System.out.println("\u001B[32m" + "\n" + "Congrats Player 2! You have won!" + "\u001B[0m");
		
		
	}
}