public class Card{
    private Suits cardSuit;
    private Ranks cardValue;

    public Card(Suits cardSuit, Ranks cardValue){
        this.cardSuit = cardSuit;
        this.cardValue = cardValue;
    }

    public Suits getSuit(){
        return this.cardSuit;
    }

    public Ranks getValue(){
        return this.cardValue;
    }

    public String toString(){
		
		//completely redundant, just get fields and convert it from an enum to a string		
        return this.cardValue.toString() + " of " + this.cardSuit.toString();
    }
	
	public double calculateScore(){
		
		//trivialized by getScore() function in enums
		return cardSuit.getScore() + cardValue.getScore();
	}
} 