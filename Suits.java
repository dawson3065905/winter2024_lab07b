enum Suits{
    CLUBS(0.1), 
    DIAMONDS(0.2), 
    SPADES(0.3),
    HEARTS(0.4);

    //fields
    private double score;

	//used to set a constant double value
    private Suits(double score){
        this.score = score;
    }

    public double getScore(){
        return this.score;
    }
}
